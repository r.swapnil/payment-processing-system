# payment-processing-system

## Getting started

To make it easy for you to get started with the project, here's a brief information about the project.

## Description
payment-processing-system is a microservice with REST API architecture. Uses a default mongoDb connection provided by spring-boot-starter-data-mongodb dependency for persistence.

Here are some properties related to mongoDb connection already quoted in application.properties file.

spring.data.mongodb.host=localhost

spring.data.mongodb.port=27017

spring.data.mongodb.database=ppc-data

The application makes use of 3 document type Db collections under ppc-data database.
1. employee (stores all employee related data which got created as result of event transactions specified in comma separated file)
2. eventStore (stores all event related data for tracking transactions specific to a particular employee in comma separated file)
3. sequence (stores the sequence number for tracking the eventnumber in sequential order)

Contains two REST endpoints:
1. POST http://localhost:8080/ppc/exec

This endpoint accepts a multipart file as a request body (you can have a comma separated .txt file or a standard .csv for this one).
After receiving the file, the API reads the file according to the data points specified in the assignment documentation and processes the same for performing CRUD operations on employee database.

2. GET http://localhost:8080/ppc/generateReport

This is a GET REST endpoint to consolidate all the employee related information and generate a report file according to the persisted event transactions which were specified in the csv file.

## Installation
Take clone of the repository from the main branch. Once done, open the application using IDE (IntelliJ, Eclipse). 
Wait for the maven build to complete. pom.xml contains all the list of dependencies required to build the project.
Application uses a default mongoDb driver provided by spring-boot-starter-data-mongodb dependency for persistence.
If you want to take a look at the data which is getting persisted as the result of csv operations, you can see them with the help of MongoDB compass.

For verification (optional):
On opening mongo DB compass, click on "New Connection +".
Enter URI as "mongodb://localhost:27017". Click on save on connect. You should be able to access localhost DBs now.
Under ppc-data database, you should be able to see 3 collections namely employee, eventStore, sequence.
These will get created automatically once you perform POST operation on csv file for data persistence.

Make sure that you have postman installed on machine to make a call to REST endpoints.

## Usage
Make sure that you have your csv file ready and in place.
Here is an example of a valid csv file according to the data points mentioned in the instructions.

1, emp101, Bill, Gates, Software Engineer, ONBOARD, 1-11-2022, 10-10-2022, Bill Gates is going to join DataOrb on 1st November as a SE. 

2, emp101, , , , EXIT, 1-10-2022, 10-10-2022, Bill Gates has exited DataOrb on 1st November as a SE.

3, emp103, Martin, Fowler, Software Engineer, ONBOARD, 15-10-2022, 10-10-2022, Martin has joined DataOrb as a SE.

4, emp103, , , , SALARY, 3000, 10-10-2022, Oct Salary of Martin.

5, emp103, , , , BONUS, 3000, 10-10-2022, Yearly bonus of Martin.

6, emp103, , , , REIMBURSEMENT, 3000, 10-10-2022, Oct reimbursement of Martin.

Important: Please take note here that events apart from ONBOARD don't really require any employee info specific values (empFName, empLName, designation) to be read from file and hence they can be ignored. 
Empty values must be comma separated accordingly. Please take above example as a reference for constructing a valid csv file.
Each statement follows a data point format of (sequenceNo, empId, empFName, empLName, designation, event, eventValue, eventDate, notes).

Once you have the csv in place, use it in postman to hit the below endpoint with that csv file as a requestBody (form-data) ((key) file (name):  file (select from dropdown))  ((value) (select your csv file) )

POST http://localhost:8080/ppc/exec:
200 status code should indicate the successful insertion of data.

After this, to generate the report from the data persisted, hit below endpoint.
GET http://localhost:8080/ppc/generateReport:
This endpoint creates a .txt file named "output_report.txt" in the project working space which contains the requested data.

## Notes
Application provides the basic functionality keeping in mind:

a. Import CSVRead operations
b. Generation of an exported output file
c. Object oriented programming
d. SOLID principles
e. Validations on event
f. Design pattern and principles
g. Modularity and conciseness of the business logic
h. Exception handling through controllerAdvice

Some major improvements that can be done:

1. Possibility of converting the present architecture into CQRS. (Updations would be handled through commands and read operation would be done through queries)
2. More thoroughly written test cases covering all positive and negative scenarios encountered. 
3. Implementation of spring security feature to make sure that apis don't have unauthorised access.
4. Fallback mechanism in case of an invalid csv file entry. 

## Roadmap
If you have ideas for releases in the future, it is a good idea to list them in the README.
package com.ppc.paymentprocessingsystem.service;

import com.opencsv.CSVReader;
import com.ppc.paymentprocessingsystem.entity.Employee;
import com.ppc.paymentprocessingsystem.entity.EventStore;
import com.ppc.paymentprocessingsystem.helper.EventValidationHelper;
import com.ppc.paymentprocessingsystem.helper.SequenceGeneratorHelper;
import com.ppc.paymentprocessingsystem.repo.EmployeeRepository;
import com.ppc.paymentprocessingsystem.repo.EventStoreRepository;
import com.ppc.paymentprocessingsystem.service.impl.PpcServiceImpl;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.web.multipart.MultipartFile;

import java.io.ByteArrayInputStream;
import java.util.Collections;
import java.util.List;

@RunWith(MockitoJUnitRunner.class)
public class PpcServiceImplTest {

    @InjectMocks
    private PpcServiceImpl ppcService;

    @Mock
    private EmployeeRepository employeeRepository;

    @Mock
    private EventStoreRepository eventStoreRepository;

    @Mock
    private SequenceGeneratorHelper sequenceGeneratorHelper;

    @Mock
    private EventValidationHelper eventValidationHelper;

    @Test
    public void testExec() throws Exception {
        // Mocking
        MultipartFile mockFile = Mockito.mock(MultipartFile.class);
        CSVReader mockCsvReader = Mockito.mock(CSVReader.class);

        Mockito.when(mockFile.getInputStream()).thenReturn(new ByteArrayInputStream("1,emp101,Bill,Gates,Software Engineer,ONBOARD,1-11-2022,10-10-2022,Bill Gates is going to join DataOrb on 1st November as a SE.".getBytes()));
        Mockito.when(mockCsvReader.readAll()).thenReturn(Collections.singletonList(new String[]{"1", "emp101", "Bill", "Gates", "Software Engineer", "ONBOARD", "1-11-2022", "10-10-2022", "Bill Gates is going to join DataOrb on 1st November as a SE."}));

        // Executing the method
        ppcService.exec(mockFile);

        // Verifying the behavior
        Mockito.verify(employeeRepository, Mockito.times(1)).findById(Mockito.anyString());
    }

    @Test
    public void testGenerateReports() {
        // Mocking
        List<EventStore> mockEventStoreList = Collections.singletonList(Mockito.mock(EventStore.class));
        List<Employee> mockEmployeeList = Collections.singletonList(Mockito.mock(Employee.class));

        Mockito.when(eventStoreRepository.findAll()).thenReturn(mockEventStoreList);
        Mockito.when(employeeRepository.findAll()).thenReturn(mockEmployeeList);
        Mockito.when(employeeRepository.findByDateOfExit(Mockito.isNull())).thenReturn(mockEmployeeList);

        // Executing the method
        ppcService.generateReports();

        // Verifying the behavior
        Mockito.verify(eventStoreRepository, Mockito.times(1)).findAll();
        Mockito.verify(employeeRepository, Mockito.times(1)).findAll();
        Mockito.verify(employeeRepository, Mockito.times(1)).findByDateOfExit(Mockito.isNull());
    }
}
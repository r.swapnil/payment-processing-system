package com.ppc.paymentprocessingsystem.controller;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

@WebMvcTest(PpcController.class)
public class PpcControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Test
    void testReadAndExec() throws Exception {
        MockMultipartFile file = new MockMultipartFile("file", "test.txt", "text/plain", "Hello, World!".getBytes());

        mockMvc.perform(MockMvcRequestBuilders.multipart("/ppc/exec")
                .file(file)
                .contentType(MediaType.MULTIPART_FORM_DATA))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().json("{\"message\":\"File uploaded. Data entered successfully\",\"status\":200}"));
    }

    @Test
    void testReportGeneration() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/ppc/generateReport"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().json("{\"message\":\"Report generated successfully\",\"status\":200}"));
    }
}
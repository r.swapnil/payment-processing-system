package com.ppc.paymentprocessingsystem.enums;

public enum Event {

    ONBOARD, //Date of Joining in MM-DD-YYYY format
    SALARY, //Salary Amount in USD
    BONUS, //Bonus Amount in USD
    EXIT, //Date of Leaving in MM-DD-YYYY format
    REIMBURSEMENT  //Reimbursement Amount in USD
}

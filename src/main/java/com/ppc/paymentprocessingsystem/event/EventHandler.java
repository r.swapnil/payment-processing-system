package com.ppc.paymentprocessingsystem.event;

import com.ppc.paymentprocessingsystem.entity.Employee;
import com.ppc.paymentprocessingsystem.entity.EventStore;
import com.ppc.paymentprocessingsystem.enums.Event;
import com.ppc.paymentprocessingsystem.exceptions.PpcException;
import com.ppc.paymentprocessingsystem.helper.EventValidationHelper;
import com.ppc.paymentprocessingsystem.helper.SequenceGeneratorHelper;
import com.ppc.paymentprocessingsystem.repo.EmployeeRepository;
import com.ppc.paymentprocessingsystem.repo.EventStoreRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;
import java.util.UUID;

@Component
@Slf4j
public class EventHandler {

    final EmployeeRepository employeeRepository;
    final EventStoreRepository eventStoreRepository;
    final SequenceGeneratorHelper sequenceGeneratorHelper;
    final EventValidationHelper eventValidationHelper;

    public EventHandler(EmployeeRepository employeeRepository, EventStoreRepository eventStoreRepository, SequenceGeneratorHelper sequenceGeneratorHelper, EventValidationHelper eventValidationHelper) {
        this.employeeRepository = employeeRepository;
        this.eventStoreRepository = eventStoreRepository;
        this.sequenceGeneratorHelper = sequenceGeneratorHelper;
        this.eventValidationHelper = eventValidationHelper;
    }

    // Methods to handle events like ONBOARD, SALARY, EXIT, etc.
    public void processEvent(Employee employee, List<String> fields, String event, String eventValue, Date eventDate) throws ParseException {
        String timeZone = "Asia/Kolkata";
        switch (event) {
            case "ONBOARD":
                // Handle ONBOARD event
                SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
                sdf.setTimeZone(TimeZone.getTimeZone(timeZone));
                Date parsedDate = sdf.parse(eventValue);
                employee.setDateOfJoining(parsedDate);
                employee.setDateOfExit(null);
                employee.setNotes(fields.get(8));
                Employee savedEmployee = employeeRepository.save(employee);
                prepareEventAndPublishToEventStore(savedEmployee, Event.ONBOARD, eventValue, eventDate);
                break;
            case "SALARY":
                //validate event first
                boolean validSalaryEvent = eventValidationHelper.validate(employee.getEmpID(), Event.SALARY, eventDate);
                if (validSalaryEvent) {
                    // Handle SALARY event
                    employee.setSalary(employee.getNonNullSalary() + Double.parseDouble(eventValue));
                    employee.setTotalAmount(employee.getNonNullSalary() + employee.getNonNullBonus() + employee.getNonNullReimbursement());
                    employee.setNotes(fields.get(8));
                    Employee savedSalaryOfEmployee = employeeRepository.save(employee);
                    prepareEventAndPublishToEventStore(savedSalaryOfEmployee, Event.SALARY, eventValue, eventDate);
                    log.info("Salary has been credited");
                    break;
                } else {
                    throw new PpcException("Salary has already been credited for the particular month");
                }
            case "BONUS":
                //validate event first
                boolean validBonusEvent = eventValidationHelper.validate(employee.getEmpID(), Event.BONUS, eventDate);
                if (validBonusEvent) {
                    // Handle SALARY event
                    employee.setBonus(employee.getNonNullBonus() + Double.parseDouble(eventValue));
                    employee.setTotalAmount(employee.getNonNullSalary() + employee.getNonNullBonus() + employee.getNonNullReimbursement());
                    employee.setNotes(fields.get(8));
                    Employee savedBonusOfEmployee = employeeRepository.save(employee);
                    prepareEventAndPublishToEventStore(savedBonusOfEmployee, Event.BONUS, eventValue, eventDate);
                    log.info("Bonus has been credited");
                    break;
                } else
                    throw new PpcException("Bonus has already been credited for the particular year");
            case "EXIT":
                    // Handle EXIT event
                    SimpleDateFormat sdf2 = new SimpleDateFormat("dd-MM-yyyy");
                    sdf2.setTimeZone(TimeZone.getTimeZone(timeZone));
                    Date parsedDateExit = sdf2.parse(eventValue);
                    employee.setDateOfExit(parsedDateExit);
                    employee.setNotes(fields.get(8));
                    Employee savedEmployeeExit = employeeRepository.save(employee);
                    prepareEventAndPublishToEventStore(savedEmployeeExit, Event.EXIT, eventValue, eventDate);
                    break;
            case "REIMBURSEMENT":
                //validate event first
                boolean validReimbursementEvent = eventValidationHelper.validate(employee.getEmpID(), Event.REIMBURSEMENT, eventDate);
                if (validReimbursementEvent) {
                    log.info("REIMBURSEMENT has been credited");
                    // Handle SALARY event
                    employee.setReimbursement(employee.getNonNullReimbursement() + Double.parseDouble(eventValue));
                    employee.setTotalAmount(employee.getNonNullSalary() + employee.getNonNullBonus() + employee.getNonNullReimbursement());
                    employee.setNotes(fields.get(8));
                    Employee savedReimbursementOfEmployee = employeeRepository.save(employee);
                    prepareEventAndPublishToEventStore(savedReimbursementOfEmployee, Event.REIMBURSEMENT, eventValue, eventDate);
                    break;
                }
            default:
                throw new PpcException("Invalid Payroll operation type detected");
        }
    }

    private void prepareEventAndPublishToEventStore(Employee emp, Event event, String value, Date eventDate) {
        EventStore ppcEvent = new EventStore();
        ppcEvent.setEventId(UUID.randomUUID().toString());
        ppcEvent.setEventNumber(sequenceGeneratorHelper.generateSequence(EventStore.SEQUENCE_NAME));
        ppcEvent.setEmpID(emp.getEmpID());
        ppcEvent.setEvent(event);
        ppcEvent.setValue(value);
        ppcEvent.setEventDate(eventDate);
        ppcEvent.setNotes(emp.getNotes());
        eventStoreRepository.save(ppcEvent);
    }

}

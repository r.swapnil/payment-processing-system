package com.ppc.paymentprocessingsystem.service.impl;

import com.opencsv.CSVReader;
import com.opencsv.exceptions.CsvException;
import com.ppc.paymentprocessingsystem.entity.Employee;
import com.ppc.paymentprocessingsystem.entity.EventStore;
import com.ppc.paymentprocessingsystem.enums.Event;
import com.ppc.paymentprocessingsystem.event.EventHandler;
import com.ppc.paymentprocessingsystem.exceptions.PpcException;
import com.ppc.paymentprocessingsystem.helper.EventValidationHelper;
import com.ppc.paymentprocessingsystem.helper.SequenceGeneratorHelper;
import com.ppc.paymentprocessingsystem.repo.EmployeeRepository;
import com.ppc.paymentprocessingsystem.repo.EventStoreRepository;
import com.ppc.paymentprocessingsystem.service.PpcService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.*;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;

@Service
@Slf4j
public class PpcServiceImpl implements PpcService {

    //NOTE: the generated .txt report file will get generated with name "output_report.txt" in the project directory itself
    private static final String OUTPUT_FILE_PATH = "output_report.txt";

    private static final String timeZone = "Asia/Kolkata";

    @Autowired
    public EmployeeRepository employeeRepository;
    @Autowired
    public EventStoreRepository eventStoreRepository;
    @Autowired
    private SequenceGeneratorHelper sequenceGeneratorHelper;
    @Autowired
    private EventValidationHelper eventValidationHelper;

    private List<EventStore> eventsList = new ArrayList<>();

    @Override
    public void exec(MultipartFile file) throws Exception {
        try {
            //first read csv and separate it into a list of arrays containing comma separated strings
            List<String[]> csvData = readCsv(file);

            //process the employee record information that we got from csvData
            processEmployeeRecord(csvData);

            //generateReports();
        } catch (IOException e) {
            throw new PpcException("Error reading CSV file: " + e.getMessage());
        }
    }

    private List<String[]> readCsv(MultipartFile file) throws IOException, CsvException {
        try (CSVReader csvReader = new CSVReader(new InputStreamReader(file.getInputStream()))) {
            return csvReader.readAll();
        }
    }

    private void processEmployeeRecord(List<String[]> records) throws ParseException {
        // Parse the employee record and create Employee objects
        /*1, emp101, Bill, Gates, Software Engineer, ONBOARD, 1-11-2022, 10-10-2022, “Bill Gates is going to join DataOrb on 1st November as a SE.”
            2, emp102, Steve Jobs, Architect, ONBOARD, 1-10-2022, 10-10-2022, “Steve Jobs joined DataOrb on 1st October as an Architect.”
            3, emp103, Martin, Fowler, Software Engineer, ONBOARD, 15-10-2022, 10-10-2022, “Martin has joined DataOrb as a SE.”
            4, emp102, , , , SALARY, 3000, 10-10-2022, “Oct Salary of Steve.”*/

            /*Date of Joining in MM-DD-YYYY format
            Date of Joining in MM-DD-YYYY format*/
        records.stream().forEach(record -> {
            List<String> recordValues = Arrays.stream(record).map(String::trim).collect(Collectors.toList());
            List<String> payrollOperation = Arrays.asList(String.valueOf(Event.SALARY),
                    String.valueOf(Event.BONUS),
                    String.valueOf(Event.REIMBURSEMENT));

            if(null == recordValues.get(1))
                throw new PpcException("No employee id data present in the file.");

            //ONBOARD check
            //This means that this doesn't contain any payroll operation to be performed but employee name information
            if (!payrollOperation.contains(recordValues.get(2)) && recordValues.get(5).contains(String.valueOf(Event.ONBOARD))) {
                Employee employee = new Employee();
                String empid = recordValues.get(1);
                Optional<Employee> employeeDAO = employeeRepository.findById(empid);
                if(employeeDAO.isEmpty()) {
                    employee.setSequenceNo(Long.valueOf(recordValues.get(0)));
                    employee.setEmpID(recordValues.get(1));
                    employee.setEmpFName(recordValues.get(2));
                    employee.setEmpLName(recordValues.get(3));
                    employee.setDesignation(recordValues.get(4));

                    String event = recordValues.get(5);
                    String eventValue = recordValues.get(6);
                    try {
                        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
                        sdf.setTimeZone(TimeZone.getTimeZone(timeZone));
                        Date parsedDate = sdf.parse(recordValues.get(7));
                        EventHandler handler = new EventHandler(employeeRepository, eventStoreRepository, sequenceGeneratorHelper, null);
                        handler.processEvent(employee, recordValues, event, eventValue, parsedDate);
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                }
                else
                    throw new PpcException("Employee with empID:" + recordValues.get(1) + " is already present in the database. Hence aborting onboarding");

            }
            // EXIT check
            else if (!payrollOperation.contains(recordValues.get(2)) && recordValues.get(5).contains(String.valueOf(Event.EXIT))) {
                String empid = recordValues.get(1);
                Optional<Employee> employeeDAO = employeeRepository.findById(empid);
                if(!employeeDAO.isEmpty()) {
                    Employee employee = employeeDAO.get();
                    employee.setSequenceNo(Long.valueOf(recordValues.get(0)));
                    String event = recordValues.get(5);
                    String eventValue = recordValues.get(6);
                    try {
                        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
                        sdf.setTimeZone(TimeZone.getTimeZone(timeZone));
                        Date parsedDate = sdf.parse(recordValues.get(7));
                        EventHandler handler = new EventHandler(employeeRepository, eventStoreRepository, sequenceGeneratorHelper, null);
                        handler.processEvent(employee, recordValues, event, eventValue, parsedDate);
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                }
                else
                    throw new PpcException("Employee with empID: {} is not present in the database. Hence aborting exit operation");

            }
            //PAYROLL operations check
            else if (payrollOperation.contains(recordValues.get(5))) {
                //Payroll operation
                String empid = recordValues.get(1);
                Optional<Employee> employeeDAO = employeeRepository.findById(empid);
                if(employeeDAO.isEmpty())
                    throw new PpcException("No employee data present for the given empID in the file.");
                Employee employee = employeeDAO.get();

                String event = recordValues.get(5);
                String eventValue = recordValues.get(6);
                try {
                    SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
                    sdf.setTimeZone(TimeZone.getTimeZone(timeZone));
                    Date parsedDate = sdf.parse(recordValues.get(7));
                    EventHandler handler = new EventHandler(employeeRepository, eventStoreRepository, sequenceGeneratorHelper, eventValidationHelper);
                    handler.processEvent(employee, recordValues, event, eventValue, parsedDate);
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    private Employee getEmployeeById(String empID, List<Employee> totalEmployees) {
        for (Employee employee : totalEmployees) {
            if (employee.getEmpID().equals(empID)) {
                return employee;
            }
        }
        return null;
    }

    @Override
    public void generateReports() {
        eventsList = eventStoreRepository.findAll();
        List<Employee> totalEmployees = employeeRepository.findAll();
        List<Employee> employees = employeeRepository.findByDateOfExit(null);   //active employees in the company

        try (PrintWriter writer = new PrintWriter(new FileWriter(OUTPUT_FILE_PATH))) {
            // 1. Total number of employees
            writer.println("1. Total number of active employees: " + employees.size());
            writer.println();

            // 2. Month-wise details
            Map<String, List<Employee>> joinedByMonth = new HashMap<>();
            Map<String, List<Employee>> exitedByMonth = new HashMap<>();
            List<EventStore> onboardExitEvents = eventsList.stream()
                    .filter(e-> e.getEvent().equals(Event.ONBOARD) || e.getEvent().equals(Event.EXIT)).collect(Collectors.toList());
            for (EventStore event : onboardExitEvents) {
                String monthYear = new SimpleDateFormat("MM-yyyy").format(event.getEventDate());
                if (event.getEvent().equals(Event.ONBOARD)) {
                    joinedByMonth.computeIfAbsent(monthYear, k -> new ArrayList<>())
                            .add(getEmployeeById(event.getEmpID(), totalEmployees));
                } else if (event.getEvent().equals(Event.EXIT)) {
                    exitedByMonth.computeIfAbsent(monthYear, k -> new ArrayList<>())
                            .add(getEmployeeById(event.getEmpID(), totalEmployees));
                }
            }

            writer.println("2a. Total number of employees joined the organization:");
            for (Map.Entry<String, List<Employee>> entry : joinedByMonth.entrySet()) {
                writer.println(entry.getKey() + ": " + entry.getValue().size() + " employees");
                for (Employee employee : entry.getValue()) {
                    writer.println("  - EmpID: " + employee.getEmpID() +
                            ", Name: " + employee.getEmpFName() +
                            ", Surname: " + employee.getEmpLName() +
                            ", Designation: " + employee.getDesignation());
                }
            }

            writer.println("2b. Total number of employees exit organization:");
            for (Map.Entry<String, List<Employee>> entry : exitedByMonth.entrySet()) {
                writer.println(entry.getKey() + ": " + entry.getValue().size() + " employees");
                for (Employee employee : entry.getValue()) {
                    writer.println("  - Name: " + employee.getEmpFName() +
                            ", Surname: " + employee.getEmpLName());
                }
            }

            // 3. Monthly salary report
            Map<String, Double> totalSalaryByMonth = new HashMap<>();
            Map<String, Integer> totalEmployeesByMonth = new HashMap<>();
            List<EventStore> salaryEvents = eventsList.stream()
                    .filter(e-> e.getEvent().equals(Event.SALARY)).collect(Collectors.toList());

            for (EventStore event : salaryEvents) {
                if (event.getEvent().equals(Event.SALARY)) {
                    String monthYear = new SimpleDateFormat("MM-yyyy").format(event.getEventDate());
                    totalSalaryByMonth.merge(monthYear, Double.parseDouble(event.getValue()), Double::sum);
                    totalEmployeesByMonth.merge(monthYear, 1, Integer::sum);
                }
            }

            writer.println();
            writer.println("3. Monthly salary report:");
            for (Map.Entry<String, Double> entry : totalSalaryByMonth.entrySet()) {
                writer.println(entry.getKey() + ": " +
                        "Total Salary: $" + entry.getValue() +
                        ", Total employees: " + totalEmployeesByMonth.get(entry.getKey()));
            }

            // 4. Employee wise financial report
            Map<String, Double> totalAmountPaidByEmployee = new HashMap<>();
            for (Employee employee : employees) {
                double totalAmountPaid = totalAmountPaidByEmployee.getOrDefault(employee.getEmpID(), 0.0);
                totalAmountPaid += employee.getNonNullSalary() + employee.getNonNullBonus() + employee.getNonNullReimbursement();
                totalAmountPaidByEmployee.put(employee.getEmpID(), totalAmountPaid);
            }

            writer.println();
            writer.println("4. Employee wise financial report:");
            for (Map.Entry<String, Double> entry : totalAmountPaidByEmployee.entrySet()) {
                Employee employee = getEmployeeById(entry.getKey(), totalEmployees);
                if (employee != null) {
                    writer.println("  - EmpID: " + entry.getKey() +
                            ", Name: " + employee.getEmpFName() +
                            ", Surname: " + employee.getEmpLName() +
                            ", Total amount paid: $" + entry.getValue());
                }
            }

           /* // 5. Monthly amount released
            Map<String, Double> totalAmountReleasedByMonth = new HashMap<>();
            Map<String, Integer> totalEmployeesReleasedByMonth = new HashMap<>();
            for (Employee employee : employees) {
                String monthYear = new SimpleDateFormat("MM-yyyy").format(employee.getEventDate());
                double totalAmount = totalAmountReleasedByMonth.getOrDefault(monthYear, 0.0);
                totalAmount += employee.getSalary() + employee.getBonus() + employee.getReimbursement();
                totalAmountReleasedByMonth.put(monthYear, totalAmount);
                totalEmployeesReleasedByMonth.merge(monthYear, 1, Integer::sum);
            }

            writer.println("5. Monthly amount released:");
            for (Map.Entry<String, Double> entry : totalAmountReleasedByMonth.entrySet()) {
                writer.println(entry.getKey() + ": " +
                        "Total Amount: $" + entry.getValue() +
                        ", Total employees: " + totalEmployeesReleasedByMonth.get(entry.getKey()));
            }*/

            // 6. Yearly financial report
            writer.println();
            writer.println("6. Yearly financial report:");
            List<EventStore> salaryBonusReimbursement = eventsList.stream()
                    .filter(e-> e.getEvent().equals(Event.SALARY) || e.getEvent().equals(Event.BONUS) || e.getEvent().equals(Event.REIMBURSEMENT))
                    .collect(Collectors.toList());
            for (EventStore event : salaryBonusReimbursement) {
                writer.println("  - Event: " + event.getEvent() +
                        ", Emp Id: " + event.getEmpID() +
                        ", Event Date: " + new SimpleDateFormat("dd-MM-yyyy").format(event.getEventDate()) +
                        ", Event Value: " + event.getValue());
            }

            // Export the report to a text file
            System.out.println("Report exported to: " + OUTPUT_FILE_PATH);
        } catch (IOException e) {
            e.printStackTrace();
            System.err.println("Error exporting report to a text file: " + e.getMessage());
        }
    }
}

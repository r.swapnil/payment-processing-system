package com.ppc.paymentprocessingsystem.service;

import org.springframework.web.multipart.MultipartFile;

public interface PpcService {

    void exec(MultipartFile file) throws Exception;

    void generateReports();

}

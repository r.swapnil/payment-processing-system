package com.ppc.paymentprocessingsystem.controller;

import com.ppc.paymentprocessingsystem.response.CmnResponse;
import com.ppc.paymentprocessingsystem.service.impl.PpcServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

@RestController
@RequestMapping("/ppc")
public class PpcController {

    @Autowired
    PpcServiceImpl ppcServiceImpl;

    @PostMapping("/exec")
    public ResponseEntity<CmnResponse> readAndExec(@RequestParam(value = "file", required = true) MultipartFile file) throws Exception {
        ppcServiceImpl.exec(file);
        CmnResponse resp=new CmnResponse("File uploaded. Data entered successfully", HttpStatus.OK.value());
        return new ResponseEntity<CmnResponse>(resp, HttpStatus.OK);
    }

    @GetMapping("/generateReport")
    public ResponseEntity<CmnResponse> reportGeneration() throws Exception {
        ppcServiceImpl.generateReports();
        CmnResponse resp=new CmnResponse("Report generated successfully", HttpStatus.OK.value());
        return new ResponseEntity<CmnResponse>(resp, HttpStatus.OK);
    }
}
package com.ppc.paymentprocessingsystem;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;

@SpringBootApplication
@ComponentScan
@Slf4j
@EnableMongoRepositories
public class PaymentProcessingSystemApplication {

	public static void main(String[] args) {
		SpringApplication.run(PaymentProcessingSystemApplication.class, args);
		log.info("*************************************************************");
		log.info("************* PPC Service successfully started **************");
		log.info("*************************************************************");
	}
}

package com.ppc.paymentprocessingsystem.controllerAdvice;

import com.ppc.paymentprocessingsystem.exceptions.PpcException;
import com.ppc.paymentprocessingsystem.response.CmnResponse;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import java.util.LinkedHashMap;
import java.util.Map;

@ControllerAdvice
public class GlobalExceptionHandler {

    @ExceptionHandler(Exception.class)
    public ResponseEntity<CmnResponse> genericAttachmentException(PpcException ex)
    {
        return new ResponseEntity<>(new CmnResponse(ex.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR.value()),
                                                HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @ExceptionHandler(MethodArgumentNotValidException.class)
    public ResponseEntity<Object> handleMethodArgueMentValidation(MethodArgumentNotValidException ex){
        Map<String,String> errorMap = new LinkedHashMap<>();
        ex.getBindingResult().getAllErrors().stream().forEach(e ->{
            String fieldName = ((FieldError) e).getField();
            String errorMessage = e.getDefaultMessage();
            errorMap.put(fieldName,errorMessage);
        });
        return new ResponseEntity<Object>(errorMap,HttpStatus.BAD_REQUEST);
    }
}

package com.ppc.paymentprocessingsystem.entity;

import com.ppc.paymentprocessingsystem.enums.Event;
import com.ppc.paymentprocessingsystem.helper.SequenceGeneratorHelper;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import java.text.SimpleDateFormat;
import java.util.Date;

@Data
@NoArgsConstructor
@Document(collection = "eventStore")
public class EventStore {
    public static final String SEQUENCE_NAME = "PPC_SEQUENCE";

    // attributes of an event
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private ObjectId id;
    private Long eventNumber;
    private String eventId;
    private String empID;
    private Event event;
    private String value;
    private Date eventDate; // Date attribute with "dd-mm-yyyy" format
    private String notes;

    @Autowired
    private SequenceGeneratorHelper sequenceGeneratorHelper;

    public EventStore(ObjectId id, String eventId, String empID, Event event, String value, Date eventDate, String notes, SequenceGeneratorHelper sequenceGeneratorHelper) {
        this.id = id;
        this.eventNumber = sequenceGeneratorHelper.generateSequence(EventStore.SEQUENCE_NAME);
        this.eventId = eventId;
        this.empID = empID;
        this.event = event;
        this.value = value;
        this.eventDate = eventDate;
        this.notes = notes;
        this.sequenceGeneratorHelper = sequenceGeneratorHelper;
    }

    public String getFormattedEventDate() {
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
        return dateFormat.format(this.eventDate);
    }

}
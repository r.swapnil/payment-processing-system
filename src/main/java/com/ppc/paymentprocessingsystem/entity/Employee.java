package com.ppc.paymentprocessingsystem.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import java.util.Date;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Document(collection = "employee")
public class Employee {
    // attributes of an employee
    private Long sequenceNo;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private String empID;
    private String empFName;
    private String empLName;
    private String designation;
    private Date dateOfJoining;
    private Date dateOfExit;
    private Double salary;  //monthly
    private Double bonus;   //yearly
    private Double reimbursement;  //monthly
    private Double totalAmount;  //yearly
    private String notes;

    public double getNonNullSalary() {
        return salary != null ? salary : 0.0; // or any default value you prefer
    }
    public double getNonNullBonus() {
        return bonus != null ? bonus : 0.0; // or any default value you prefer
    }
    public double getNonNullReimbursement() {
        return reimbursement != null ? reimbursement : 0.0; // or any default value you prefer
    }
    public double getNonNullTotalAmount() {
        return totalAmount != null ? totalAmount : 0.0; // or any default value you prefer
    }

}

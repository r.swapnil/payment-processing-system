package com.ppc.paymentprocessingsystem.helper;

import com.ppc.paymentprocessingsystem.entity.Sequence;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.FindAndModifyOptions;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Component;

@Component
public class SequenceGeneratorHelper {

    @Autowired
    private MongoOperations mongoOperations;

    //for maintaining the event number sequence in our eventStore
    public Long generateSequence(String seqName) {
        Sequence counter = mongoOperations.findAndModify(
                Query.query(Criteria.where("_id").is(seqName)),
                new Update().inc("seq", 1),
                FindAndModifyOptions.options().returnNew(true).upsert(true),
                Sequence.class);
        return counter.getSeq();
    }
}

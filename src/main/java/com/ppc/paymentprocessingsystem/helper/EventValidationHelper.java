package com.ppc.paymentprocessingsystem.helper;

import com.ppc.paymentprocessingsystem.entity.EventStore;
import com.ppc.paymentprocessingsystem.enums.Event;
import com.ppc.paymentprocessingsystem.repo.EventStoreRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@Component
public class EventValidationHelper {

    @Autowired
    private EventStoreRepository eventStoreRepository;

    public boolean validate(String empId,Event event, Date eventDate) {
        if (event == null) {
            // Event is null, not valid
            return false;
        }

        switch (event) {
            case SALARY:
                return validateSalaryEvent(empId, event,eventDate);
            case BONUS:
                return validateBonusEvent(empId, event, eventDate);
            case EXIT:
                return validateExitEvent(empId, event);
            case REIMBURSEMENT:
                return validateReimbursementEvent(empId, event, eventDate);
            default:
                // Unknown event type, not valid
                return false;
        }
    }

    private boolean validateSalaryEvent(String empId, Event event, Date eventDate) {
        // Validate salary event logic
        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
        String pattern = sdf.format(eventDate).substring(2);
        List<EventStore> validSalaryEvents = eventStoreRepository.findByEmpIDAndEventAndEventDateRegexIgnoreCase(empId, event, pattern);
        if(!validSalaryEvents.isEmpty())
        return false;
        else return true;
    }

    private boolean validateBonusEvent(String empId, Event event, Date eventDate) {
        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
        String pattern = sdf.format(eventDate).substring(5);
        List<EventStore> validBonusEvents = eventStoreRepository.findByEmpIDAndEventAndEventDateRegexIgnoreCase(empId, event, pattern);
        if(!validBonusEvents.isEmpty())
            return false;
        else return true;
    }

    private boolean validateExitEvent(String empId, Event event) {
        // Validate exit event logic
        Optional<EventStore> exitEventCheck = eventStoreRepository.findByEmpIDAndEvent(empId, event);
        if(!exitEventCheck.isEmpty())
            return false;
        else return true;
    }

    private boolean validateReimbursementEvent(String empId, Event event, Date eventDate) {
        // Validate reimbursement event logic
        return true;
    }


}

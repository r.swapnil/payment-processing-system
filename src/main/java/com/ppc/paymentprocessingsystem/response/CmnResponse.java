package com.ppc.paymentprocessingsystem.response;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class CmnResponse {
    private String message;
    private int statusCode;
}

package com.ppc.paymentprocessingsystem.exceptions;

public class PpcException extends RuntimeException{
    public PpcException(String message) {
        super(message);
    }

    public PpcException(String message, Throwable cause) {
        super(message, cause);
    }

    @Override
    public String getMessage() {
        return super.getMessage();
    }
}

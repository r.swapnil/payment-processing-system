package com.ppc.paymentprocessingsystem.repo;

import com.ppc.paymentprocessingsystem.entity.EventStore;
import com.ppc.paymentprocessingsystem.enums.Event;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface EventStoreRepository extends MongoRepository<EventStore, String> {
    List<EventStore> findByEmpIDAndEventAndEventDateRegexIgnoreCase(String empID, Event event, String pattern); //for finding out the monthly or yearly occurrences of a particular event

    Optional<EventStore> findByEmpIDAndEvent(String empId, Event event);

    List<EventStore> findByEventIn(List<Event> events);
}
